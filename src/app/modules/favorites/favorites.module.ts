import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatSortModule,
  MatListModule,
  MatCardModule,
  MatInputModule,
  MatTableModule,
  MatButtonModule,
  MatFormFieldModule,
} from '@angular/material';

import { PageComponent } from './components/page/page.component';
import { FavoriteListComponent } from './components/favorite-list/favorite-list.component';
import { FavoriteMessagesComponent } from './components/favorite-messages/favorite-messages.component';
import { FavoriteMessageFormComponent } from './components/favorite-message-form/favorite-message-form.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MatListModule,
    MatIconModule,
    MatSortModule,
    MatCardModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatFormFieldModule
  ],
  declarations: [PageComponent, FavoriteListComponent, FavoriteMessagesComponent, FavoriteMessageFormComponent],
  exports: [PageComponent],
})
export class FavoritesModule { }
