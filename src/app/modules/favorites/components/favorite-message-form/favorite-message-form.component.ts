import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-favorite-message-form',
  templateUrl: './favorite-message-form.component.html',
  styleUrls: ['./favorite-message-form.component.scss']
})
export class FavoriteMessageFormComponent {
  message: string;

  @Output() send = new EventEmitter<string>();

  constructor () {
    this.message = '';
  }

  handleInput (e: any) {
    this.message = e.target.value;
  }

  handleSubmit (e: Event) {
    e.preventDefault();

    if (this.message) {
      this.send.emit(this.message);
      this.message = '';
    }
  }
}
