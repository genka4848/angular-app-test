import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteMessageFormComponent } from './favorite-message-form.component';

describe('FavoriteMessageFormComponent', () => {
  let component: FavoriteMessageFormComponent;
  let fixture: ComponentFixture<FavoriteMessageFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteMessageFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteMessageFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
