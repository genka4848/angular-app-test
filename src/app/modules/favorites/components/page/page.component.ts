import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { FavoriteService } from '../../../../common/services/favorite.service';
import { Favorite } from '../../../../common/interfaces/favorite.model';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit, OnDestroy {
  favorites: Favorite[];
  selectedFavorite: Favorite;
  favoriteSubscription: Subscription;

  constructor (private favoriteService: FavoriteService) {}

  ngOnInit () {
    this.favoriteSubscription = this.favoriteService
        .favorites$
        .subscribe(data => {
          this.favorites = data;

          if (this.selectedFavorite) {
            const newSelected = this.favorites.find(f => f.id === this.selectedFavorite.id);
            this.selectedFavorite = newSelected || null;
          }
        });
  }

  ngOnDestroy () {
    this.favoriteSubscription.unsubscribe();
  }

  handleRemoveFavorite (favorite: Favorite) {
    this.favoriteService.remove(favorite.id);
  }

  handleSelectFavorite (favorite: Favorite) {
    this.selectedFavorite = favorite;
  }

  handleSendMessage ({ favoriteId, message }) {
    this.favoriteService.addMessage(favoriteId, message);
  }

  handleClose () {
    this.selectedFavorite = null;
  }
}
