import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';

import { Favorite} from '../../../../common/interfaces/favorite.model';

@Component({
  selector: 'app-favorite-list',
  templateUrl: './favorite-list.component.html',
  styleUrls: ['./favorite-list.component.scss']
})
export class FavoriteListComponent implements OnInit {
  dataSource = new MatTableDataSource([]);
  displayedColumns: string[] = ['name', 'owner', 'providerName', 'action'];

  @Input()
  set favorites(favorites: Favorite[]) {
    this.dataSource.data = favorites || [];
  }

  @Output() remove = new EventEmitter<Favorite>();
  @Output() select = new EventEmitter<Favorite>();

  @ViewChild(MatSort) sort: MatSort;

  constructor() { }

  ngOnInit() {
    this.dataSource.sort = this.sort;
  }

  handleRowRemove (row: Favorite) {
    this.remove.emit(row);
  }

  handleRowSelect (row: Favorite) {
    this.select.emit(row);
  }
}
