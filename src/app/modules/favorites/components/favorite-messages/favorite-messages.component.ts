import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Favorite } from '../../../../common/interfaces/favorite.model';

@Component({
  selector: 'app-favorite-messages',
  templateUrl: './favorite-messages.component.html',
  styleUrls: ['./favorite-messages.component.scss']
})
export class FavoriteMessagesComponent {

  @Input() favorite: Favorite;

  @Output() sendMessage = new EventEmitter<any>();

  handleSend (message: string) {
    this.sendMessage.emit({ favoriteId: this.favorite.id, message });
  }
}
