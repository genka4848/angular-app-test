import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriteMessagesComponent } from './favorite-messages.component';

describe('FavoriteMessagesComponent', () => {
  let component: FavoriteMessagesComponent;
  let fixture: ComponentFixture<FavoriteMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavoriteMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriteMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
