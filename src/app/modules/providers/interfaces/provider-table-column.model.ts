export interface ProviderTableColumn {
  column: string;
  columnName: string;
  sortDisabled: boolean;
}
