export interface ProviderItem {
  id: string;
  name: string;
  releaseDate: string;
  // For track item
  album?: string;
  artist?: string;
  // For video item
  author?: string;
  duration?: number;
  status?: string;
}
