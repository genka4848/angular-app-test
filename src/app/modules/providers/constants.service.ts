import { Provider } from '../../common/interfaces/provider.model';

export const MUSIXMATCH_PROVIDER = 'musixmatch';
export const VIMEO_PROVIDER = 'vimeo';

export const VIMEO_ACCESS_TOKEN = '0ebfc06c25c4fc5fa2e8a553be02f691';
export const MASHAPE_KEY = 'xCmtUYejUhmshBOgkQJEK6v3PPXup1XWO4ajsnLCjkStOiP775';

export const PROVIDERS: Provider[] = [
  { name: 'Tracks (Musixmatch)', key: MUSIXMATCH_PROVIDER },
  { name: 'Videos (Vimeo)', key: VIMEO_PROVIDER }
];

export const PROVIDER_TABLE_CONFIG = {
  [MUSIXMATCH_PROVIDER]: [
    {
      column: 'name',
      columnName: 'Name',
      sortDisabled: false
    },
    {
      column: 'album',
      columnName: 'Album',
      sortDisabled: true
    },
    {
      column: 'artist',
      columnName: 'Artist',
      sortDisabled: true
    },
    {
      column: 'releaseDate',
      columnName: 'Release Date',
      sortDisabled: true
    }
  ],
  [VIMEO_PROVIDER]: [
    {
      column: 'name',
      columnName: 'Name',
      sortDisabled: false
    },
    {
      column: 'author',
      columnName: 'Author',
      sortDisabled: false
    },
    {
      column: 'duration',
      columnName: 'Duration (sec)',
      sortDisabled: true
    },
    {
      column: 'status',
      columnName: 'Status',
      sortDisabled: true
    },
    {
      column: 'releaseDate',
      columnName: 'Release Date',
      sortDisabled: true
    }
  ]
};


