import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {
  MatSortModule,
  MatTableModule,
  MatSelectModule,
  MatCheckboxModule,
  MatFormFieldModule
} from '@angular/material';

import { TrackApiService } from './services/track-api.service';
import { VideoApiService } from './services/video-api.service';

import { PageComponent } from './components/page/page.component';
import { ProviderItemsComponent } from './components/provider-items/provider-items.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    // Material Modules
    MatSortModule,
    MatTableModule,
    MatSelectModule,
    MatCheckboxModule,
    MatFormFieldModule,
  ],
  providers: [VideoApiService, TrackApiService],
  declarations: [PageComponent, ProviderItemsComponent],
  exports: [PageComponent]
})
export class ProvidersModule { }
