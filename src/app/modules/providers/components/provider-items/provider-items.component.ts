import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatSort, MatTableDataSource } from '@angular/material';

import { ProviderTableColumn } from '../../interfaces/provider-table-column.model';
import { ProviderItem } from '../../interfaces/provider-item.model';

@Component({
  selector: 'app-provider-items',
  templateUrl: './provider-items.component.html',
  styleUrls: ['./provider-items.component.scss']
})
export class ProviderItemsComponent implements OnInit {
  dataSource = new MatTableDataSource();
  selection = new SelectionModel<ProviderItem>(true, []);

  @Input() displayedColumns: string[];
  @Input() columns: ProviderTableColumn[];
  @Input()
  set items (items: ProviderItem[]) {
    this.dataSource.data = items;

    this.selection.clear();

    // Reset sorting
    this.sort.active = '';
    this.sort.direction = 'asc';
  }
  @Input()
  set selectedItems (items: ProviderItem[]) {
    items.forEach(item => { this.selection.toggle(item); });
  }

  @Output() select = new EventEmitter<any>();

  @ViewChild(MatSort) sort: MatSort;

  constructor () { }

  ngOnInit () {
    this.dataSource.sort = this.sort;
  }

  handleRowCheck (item) {
    this.selection.toggle(item);
    this.select.emit({ selected: this.selection.isSelected(item), item });
  }
}
