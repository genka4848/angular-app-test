import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { TrackApiService } from '../../services/track-api.service';
import { VideoApiService } from '../../services/video-api.service';
import { ProviderItem } from '../../interfaces/provider-item.model';
import { Provider } from '../../../../common/interfaces/provider.model';
import { FavoriteService } from '../../../../common/services/favorite.service';
import { ProviderTableColumn } from '../../interfaces/provider-table-column.model';
import {
  PROVIDERS,
  PROVIDER_TABLE_CONFIG,
  VIMEO_PROVIDER,
  MUSIXMATCH_PROVIDER
} from '../../constants.service';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit, OnDestroy {
  providerServices: object;
  providerServiceSubscription: Subscription;

  providers = PROVIDERS;
  selectedProvider: Provider;
  tableColumns: ProviderTableColumn[];

  displayedDefaultColumns: string[] = ['select'];
  displayedColumns: string[] = [];

  providerItems: ProviderItem[];
  selectedProviderItems: ProviderItem[];

  constructor (
    private favoriteService: FavoriteService,
    private trackApiService: TrackApiService,
    private videoApiService: VideoApiService,
  ) {
    this.providerItems = [];
    this.selectedProviderItems = [];
    this.selectedProvider = this.providers[0];

    this.providerServices = {
      [VIMEO_PROVIDER]: this.videoApiService,
      [MUSIXMATCH_PROVIDER]: this.trackApiService,
    };
  }

  ngOnInit () {
    this.setProviderOptionsAndData();
  }

  ngOnDestroy () {
    this.providerServiceSubscription.unsubscribe();
  }

  handleProviderChange () {
    this.setProviderOptionsAndData();
  }

  handleItemSelect ({ selected, item }) {
    if (selected) {
      const favoriteToSave = {
        resourceId: item.id,
        name: item.name,
        owner: this.selectedProvider.key === VIMEO_PROVIDER ? item.author : item.artist
      };

      this.favoriteService.add(favoriteToSave, this.selectedProvider);
    } else {
      this.favoriteService.removeByResourceId(item.id);
    }
  }

  setProviderOptionsAndData () {
    this.tableColumns = PROVIDER_TABLE_CONFIG[this.selectedProvider.key];
    this.displayedColumns = [
      ...this.displayedDefaultColumns,
      ...this.tableColumns.map(item => item.column)
    ];

      this.providerServiceSubscription =
        this.providerServices[this.selectedProvider.key]
        .getList()
        .subscribe(items => {
          this.providerItems = items;

          const favorites = this.favoriteService.getItems();
          this.selectedProviderItems = [];

          favorites
            .filter(f => f.provider.key === this.selectedProvider.key)
            .forEach(f => {
              const foundedItem = items.find(item => item.id === f.resourceId);

              if (foundedItem) {
                this.selectedProviderItems.push(foundedItem);
              }
            });
        });
  }
}
