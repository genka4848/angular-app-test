import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { MASHAPE_KEY } from '../constants.service';
import { ProviderItem } from '../interfaces/provider-item.model';

@Injectable()
export class TrackApiService {
  private useMockData: boolean;

  constructor (private http: HttpClient) {
    this.useMockData = JSON.parse(localStorage.getItem('useMockData'));
  }

  getList (): Observable<ProviderItem[]> {
    if (this.useMockData) {
      return this.http
        .get('/assets/mocks/tracks.json')
        .pipe(map(this.transform));
    }

    let headers = new HttpHeaders();
    headers = headers.append('X-Mashape-Key', MASHAPE_KEY);

    let params = new HttpParams();
    params = params.append('page_size', '50');

    return this.http
      .get(
        'https://musixmatchcom-musixmatch.p.mashape.com/wsr/1.1/track.search',
        { headers, params }
      )
      .pipe(map(this.transform));
  }

  transform (items: any[]): ProviderItem[] {
    return <ProviderItem[]>items.map(item => ({
      id: item.id + '',
      name: item.track_name,
      album: item.album_name,
      artist: item.artist_name,
      releaseDate: new Date(item.first_release_date).toLocaleDateString()
    }));
  }
}
