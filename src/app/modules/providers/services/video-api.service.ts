import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ProviderItem } from '../interfaces/provider-item.model';
import { VIMEO_ACCESS_TOKEN } from '../constants.service';

@Injectable()
export class VideoApiService {
  private useMockData: boolean;

  constructor (private http: HttpClient) {
    this.useMockData = JSON.parse(localStorage.getItem('useMockData'));
  }

  getList (): Observable<ProviderItem[]> {
    if (this.useMockData) {
      return this.http
        .get('/assets/mocks/videos.json')
        .pipe(map(this.transform));
    }

    let params = new HttpParams();

    // Query param is required by Vimeo API
    params = params.append('query', 'car');
    params = params.append('per_page', '50');
    params = params.append('format', 'json');
    params = params.append('access_token', VIMEO_ACCESS_TOKEN);

    return this.http
      .get('https://api.vimeo.com/videos', { params })
      .pipe(map((res: any) => this.transform(res.data)));
  }

  transform (items: any[]): ProviderItem[] {
    return <ProviderItem[]>items.map(item => ({
      id: item.resource_key,
      name: item.name,
      author: item.user.name,
      duration: item.duration,
      status: item.status,
      releaseDate: new Date(item.release_time).toLocaleDateString()
    }));
  }
}
