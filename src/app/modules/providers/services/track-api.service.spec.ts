import { TestBed } from '@angular/core/testing';

import { TrackApiService } from './track-api.service';

describe('TrackApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrackApiService = TestBed.get(TrackApiService);
    expect(service).toBeTruthy();
  });
});
