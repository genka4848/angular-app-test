import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProvidersModule } from './modules/providers/providers.module';
import { FavoritesModule } from './modules/favorites/favorites.module';

import { PageComponent as ProvidersPageComponent } from './modules/providers/components/page/page.component';
import { PageComponent as FavoritesPageComponent } from './modules/favorites/components/page/page.component';

const routes: Routes = [
  { path: 'providers', component: ProvidersPageComponent },
  { path: 'favorites', component: FavoritesPageComponent },
  { path: '**', redirectTo: '/providers' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true }),
    ProvidersModule,
    FavoritesModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
