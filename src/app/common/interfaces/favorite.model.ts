import { Provider } from './provider.model';

export interface FavoriteMessage {
  text: string;
  date: Date;
}

export interface Favorite {
  id: number;
  name: string;
  owner: string;
  resourceId: string;
  provider: Provider;
  messages: FavoriteMessage[];
}
