export interface Provider {
  name: string;
  key: string;
}
