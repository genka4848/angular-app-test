import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Favorite } from '../interfaces/favorite.model';

const STORAGE_KEY = 'favorites';

@Injectable({
  providedIn: 'root'
})
export class FavoriteService {
  private favorites: Favorite[];
  public favorites$: BehaviorSubject<Favorite[]>;

  constructor () {
    this.restoreFromStorage();
    this.favorites$ = new BehaviorSubject([...this.favorites]);
  }

  getItems () {
    return this.favorites;
  }

  add (params, provider) {
    const id = new Date().getTime();
    const { name, owner, resourceId } = params;
    const newFavorite: Favorite = { id, name, owner, resourceId, provider, messages: [] };

    this.favorites.unshift(newFavorite);
    this.favorites$.next([...this.favorites]);
    localStorage.setItem(STORAGE_KEY, JSON.stringify(this.favorites));

    return this;
  }

  remove (id: number) {
    const index = this.favorites.findIndex(f => id === f.id);

    if (index === -1) {
      console.warn(`Favorite with id=${id} not found`);
    } else {
      this.favorites.splice(index, 1);
      this.favorites$.next([...this.favorites]);
      localStorage.setItem(STORAGE_KEY, JSON.stringify(this.favorites));
    }

    return this;
  }

  removeByResourceId (resourceId: string) {
    const index = this.favorites.findIndex(f => resourceId === f.resourceId);

    if (index === -1) {
      console.warn(`Favorite with resourceId=${resourceId} not found`);
    } else {
      this.favorites.splice(index, 1);
      this.favorites$.next([...this.favorites]);
      localStorage.setItem(STORAGE_KEY, JSON.stringify(this.favorites));
    }

    return this;
  }

  addMessage (favoriteId: number, message: string) {
    const favorite = this.favorites.find(f => favoriteId === f.id);

    if (!favorite) {
      console.warn(`Favorite with favoriteId=${favoriteId} not found`);
    } else {
      favorite.messages.push({ text: message, date: new Date() });
      this.favorites$.next([...this.favorites]);
      localStorage.setItem(STORAGE_KEY, JSON.stringify(this.favorites));
    }

    return this;
  }

  restoreFromStorage () {
    try {
      const items = JSON.parse(localStorage.getItem(STORAGE_KEY));

      this.favorites = Array.isArray(items) ? items : [];
    } catch (err) {
      console.error(err);
      this.favorites = [];
    }
  }
}
